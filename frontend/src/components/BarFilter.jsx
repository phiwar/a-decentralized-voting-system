import React, { Component } from "react";


import { Bar, Line, Pie } from "react-chartjs-2";

export default class BarFilter extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            labelData: [],
            valueData: [],
            renderedData: {}
        }
    }

    componentDidUpdate(prevProps){
        if(this.props.labelData !== prevProps.labelData){
            // alert(this.props.data);
            this.setState({
                labelData: this.props.labelData,
                valueData: this.props.valueData,
                renderedData: this.getAmount(this.props.labelData)
            });
        }
    }

    // Counts amount of different elements in list
    getAmount(list){
        var result = {};
        for(var i = 0; i < list.length; i++){
        var key = list[i];
        // Check if already a key
        if(Object.keys(result).includes(key)){
            result[key] += 1;
        }
        // If not present initate a key
        else{
            result[key] = 1;
        }
        }
        return result;
    }

    chartData(labels, data) {
        return {
          labels: labels,
          datasets: [
            {
              label: "My First dataset",
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: data,
            },
          ],
        };
      }

    // keyList contains all keys that will be filtered and
    // valueList contains values of filtered list 
    filter(keyList, valueList, k){
        var result = [];
        for(var i = 0; i < keyList.length; i++){
        if(keyList[i] === k){
            result.push(valueList[i]);
        }
        }
        return result;
    }
    renderData(event){
        if(event.target.value == "All"){
            this.setState({ 
                renderedData:this.getAmount(this.state.labelData)
            });
        }
        else{
            this.setState({ renderedData:
                this.getAmount(
                    this.filter(
                    this.state.valueData, 
                    this.state.labelData, 
                    event.target.value)
                    )
                });
        }
    }

    render(){
        if(this.state.labelData.length == 0){
            return <div></div>
        }
        else{
            return(
                <div> 
                    <select 
                        onChange={(event) => this.renderData(event)
                        }>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Non binary</option>
                            <option>All</option>
                        </select>
                    <Bar height={75} data={
                        this.chartData(
                            Object.keys(this.state.renderedData), 
                            Object.values(this.state.renderedData)
                        )}
                    />
                </div>
            )
        }
    }
}