import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Smart contract modules
import contract from "truffle-contract";
import PollJson from "./Poll.json";
import getWeb3 from "../services/getWeb3";

// Imported components
import Dashboard from "../components/Dashboard";
import Users from "../components/Users.js";
import Admin from "./../components/Admin";
import { authenticationService } from "../services/authenticationService";
import NavbarCustom from "../components/NavbarCustom";
import Home from "../components/Home";

// Bootstrap and other useful third-party components
import Sidebar from "react-sidebar";
import axios from "axios";

// CSS
import "../css/dashboard.css";
import "./../App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSidebar: true,
      storageValue: 0,
      web3: null,
      accounts: null,
      contract: null,
      qCount: 0,
      question: null,
      id: null,
      answers: { 0: [] },
      update: null,
      key: null,
      polls: [],
      titles: ["Dödstraff"],
      dashboardState: {
        id: null,
      },
      currentUser: null,
    };
  }

  componentDidMount = async () => {
    // Initialize web3
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = PollJson.networks[networkId];
      const instance = new web3.eth.Contract(
        PollJson.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance }, this.initPolls);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.error(error);
    }
    // Subscribe to changes to currentUser state
    authenticationService.currentUser.subscribe((x) =>
      this.setState({ currentUser: x })
    );
  };

  async updateDashboard(poll) {
    try {
      poll = await this.getStats(poll);
      this.setState({
        dashboardState: poll,
      });
    } catch (err) {
      console.log("Cannot fetch dashboard");
    }
  }

  sidebarContent() {
    var pollList = this.state.polls.map((poll, i) => {
      return (
        <div className={"poll-link"}>
          <a key={i} onClick={() => this.updateDashboard(poll)}>
            {poll.title} <br />
            <small>votes: {poll.voteCount}</small>
            <br />
          </a>
        </div>
      );
    });
    return (
      <div className="dashboard-sidebar">
        <h5 className="dashboard-sidebar-title">All Polls</h5>
        {pollList}
      </div>
    );
  }

  // Contract GET functions

  // Gets titles and questions for poll with poll id: pid
  async getPoll(pid) {
    const { accounts, contract } = this.state;
    var result = { id: 0, title: null, qs: [], voteCount: 0, qVoteCount: [] };
    var done = false;
    // This should initiate a transaction
    for (var i = 0; i <= 10; i++) {
      // polls can be viewed as a 2D array with the rows representing each poll and columns
      // representing the answers. This for-loop will fetch each question from the row given
      // by the poll id (pid)
      await contract.methods
        .polls(pid, i)
        .call()
        .then(function (res) {
          if (res.ans === "") {
            done = true;
          } else {
            result.qs.push(res.ans);
            result.qVoteCount.push(res.voteCount);
          }
        });
      if (done) {
        break;
      }
    }
    // pollData contains data about a poll, its id, title and vote count
    await contract.methods
      .pollData(pid)
      .call()
      .then(function (res) {
        result.id = res.id;
        result.title = res.title;
        result.voteCount = res.voteCount;
      });
    return result;
  }

  // Get all polls in the contract with ids between 0 and amount
  async getPolls(amount) {
    var polls = [];
    for (var i = 0; i < amount; i++) {
      var poll = await this.getPoll(i);
      polls.push(poll);
    }
    return polls;
  }

  async getDatapoints(pid, vcount) {
    var stats = { age: [], gender: [] };
    // For each vote present in the given poll get every datapoint
    for (var i = 0; i < vcount; i++) {
      // Get a single data point from contract
      var stat = await contract.methods
        .votes(pid, i)
        .call()
        .then(function (data) {
          // The data retrieved contains the vote id,
          // the voters address, gender and age. However,
          // more datapoints can easily be added
          return { age: data.age, gender: data.gender };
        });

      // Push datapoints into main stats json
      stats.age.push(stat.age);
      stats.gender.push(stat.gender);
    }
    return stats;
  }

  // Gets the all of the questions and the amount of votes on each
  async getStats(poll) {
    const { accounts, contract } = this.state;
    const pid = poll.id;
    // contains title, voteCount and ansCount
    var pollJSON = await this.getPoll(pid).then((res) => res);

    // Gender stats
    try {
      var genderStats = await contract.methods
        .getGender(pid)
        .call()
        .then(function (res) {
          var gender = res["0"].map(function (e) {
            if (e == 0) {
              return "Male";
            }
            if (e == 1) {
              return "Female";
            } else {
              return "Non binary";
            }
          });
          var ans = res["1"];
          return { gender: gender, voteAlts: ans };
        });
    } catch (err) {
      console.log("User does not have permissio to view stats");
      var genderStats = { gender: [], voteAlts: [] };
    }
    // alert(JSON.stringify(genderStats));
    // This is the total amount of votes for a poll
    const totVotes = pollJSON.voteCount;
    var stats = { age: [], gender: [], vote_alts: [] };

    // This is the format exprected by the Stats component
    var result = {
      id: pollJSON.id,
      title: pollJSON.title,
      qs: pollJSON.qs,
      voteCount: pollJSON.voteCount,
      qVoteCount: pollJSON.qVoteCount,
      gender: stats.gender,
      age: stats.age,
      vote_alts: stats.vote_alts,
      totStats: { voteAlts: "TODO" },
      genderStats: genderStats,
    };
    return result;
  }

  // Initate polls in state
  initPolls = async () => {
    const { accounts, contract } = this.state;
    // The getPoll function will loop through the value given to it, fetch all polls
    // between 0 and the amount given and push it into a list of JSONs.
    var tmp = await contract.methods
      .pollCount()
      .call()
      .then((amount) => this.getPolls(amount));

    this.setState({ polls: tmp });
  };

  // Casts a vote on a single question by calling the vote() function in contract.sol
  async castVote(pid, qid, gender, age, username) {
    const { accounts, contract } = this.state;
    // This should initiate a transaction
    await contract.methods
      .vote(pid, qid, gender, age)
      .send({ from: String(this.state.accounts) })
      .then(function (res) {
        const body = { value: username, pollId: pid };
        const url = "http://localhost:4000/hasVoted";
        axios
          .post(url, body)
          .then((res) => console.log("Axios request finished"));
        alert("Vote cast!");
      });
  }

  // Create a new poll with defined by titles and a list of questions
  async create(title, qs) {
    const { accounts, contract } = this.state;
    // List of answers to be provided in the poll
    await contract.methods
      .createPoll(title)
      .send({ from: String(this.state.accounts) })
      .then(async function (res) {
        const pid = await contract.methods
          .pollCount()
          .call()
          .then((id) => id - 1);
        for (var i = 0; i < qs.length; i++) {
          contract.methods
            .addPollQuestion(pid, i, qs[i])
            .send({ from: String(accounts) })
            .then(function (res) {
              console.log(qs[i] + " added into poll id: " + pid);
            });
        }
      });
  }

  render() {
    return (
      <div>
        <NavbarCustom currentUser={this.state.currentUser}></NavbarCustom>
        <Sidebar
          sidebar={this.sidebarContent()}
          docked={true}
          showSidebar={false}
          styles={{
            sidebar: {
              background: "rgb(11, 8, 51)",
              top: "9%",
              width: "15%",
            },
            root: {
              zIndex: 0,
            },
          }}
        ></Sidebar>

        {/* Routes */}
        <div className="App main-content-container">
          <Router>
            <Switch>
              <Route exact path="/home">
                <Home></Home>
              </Route>
              <Route exact path="/admin">
                <Admin
                  createFunction={(title, ans) => this.create(title, ans)}
                ></Admin>
              </Route>
              <Route exact path="/users">
                <Users></Users>
              </Route>
              <Route exact path="/">
                <Dashboard
                  id={this.state.dashboardState.id}
                  title={this.state.dashboardState.title}
                  qs={this.state.dashboardState.qs}
                  voteCount={this.state.dashboardState.voteCount}
                  qVoteCount={this.state.dashboardState.qVoteCount}
                  gender={this.state.dashboardState.gender}
                  age={this.state.dashboardState.age}
                  vote_alts={this.state.dashboardState.vote_alts}
                  // voteFunction={(pid, qid, g, a, u) => // From auth-token branch, conflict when merging.
                  //   this.castVote(pid, qid, g, a, u)      Maybe variable u is not need anymore, but kept
                  // }                                       for just in case
                  genderStats={this.state.dashboardState.genderStats}
                  voteFunction={(pid, qid, g, a) =>
                    this.castVote(pid, qid, g, a)
                  }
                  stats={() => this.getPoll(0)}
                ></Dashboard>
              </Route>
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;
