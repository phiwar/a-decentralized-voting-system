import React from "react";

// Bootstrap Components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

// Other components
import Authenticate from "./Authenticate";
import BarFilter from "./BarFilter";
import { authenticationService } from "../services/authenticationService";

// CSS
import "../css/dashboard.css";

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      id: null,
      title: null,
      qs: [],
      voteCount: [],
      qVoteCount: [],
      stats: this.props.stats,
      targetID: 0,
      gender: [],
      age: [],
      vote_alts: [],
      totStats: [],
      genderStats: {},
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        title: this.props.title,
        qs: this.props.qs,
        voteCount: this.props.voteCount,
        qVoteCount: this.props.qVoteCount,
        age: this.props.age,
        gender: this.props.gender,
        vote_alts: this.props.vote_alts,
        genderStats: this.props.genderStats,
      });
    }
  }

  chartData(labels, data) {
    return {
      labels: labels,
      datasets: [
        {
          label: "My First dataset",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: data,
        },
      ],
    };
  }

  // Counts amount of different elements in list
  getAmount(list) {
    var result = {};
    for (var i = 0; i < list.length; i++) {
      var key = list[i];
      // Check if already a key
      if (Object.keys(result).includes(key)) {
        result[key] += 1;
      }
      // If not present initate a key
      else {
        result[key] = 1;
      }
    }
    return result;
  }

  // keyList contains all keys that will be filtered and
  // valueList contains values of filtered list
  filter(keyList, valueList, k) {
    var result = [];
    for (var i = 0; i < keyList.length; i++) {
      // alert("keyList[i]: " + keyList[i]);
      // alert("k: " + k);

      if (keyList[i] === k) {
        result.push(valueList[i]);
      }
    }
    return result;
  }

  ansAlternatives() {
    alert(this.state.qs);
    var alts = this.state.qs.map((ans, i) => {
      return <option key={i}>{ans}</option>;
    });
    return alts;
  }

  vote(age, gender, username, has_voted) {
    //Add the pollID to the users "hasVoted"
    if (has_voted.includes(this.state.id)) {
      alert("You've already voted in this poll");
      return;
    }
    this.props.voteFunction(
      this.state.id,
      this.state.targetID,
      gender,
      age,
      username
    );
  }

  async stats() {
    console.log(this.state.stats());
  }

  render() {
    const seeStatsOrNot = () => {
      if (authenticationService.currentUserValue) {
        return (
          <div>
            <h2>Title of question{this.state.title}</h2>
            <h3>All votes</h3>
            <BarFilter
              labelData={this.state.vote_alts}
              valueData={this.state.gender}
            />
          </div>
        );
      } else {
        return <h3>You have to login to see statistics</h3>;
      }
    };

    return (
      <div className="bar-chart">
        <Container>
          <Row>
            {/* <Col xs={12}>{seeStatsOrNot()}</Col> from auth-token branch, conflict when merging */}
            <Col xs={12}>
              <h3>{this.state.title}</h3>
              <BarFilter
                labelData={this.state.genderStats.voteAlts}
                valueData={this.state.genderStats.gender}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <h5>Alternatives</h5>
            </Col>
          </Row>
          <Row>
            <Col>
              {this.state.qs.map((ans, i) => {
                return (
                  <Button
                    id={i}
                    onClick={(event) =>
                      this.setState({ show: true, targetID: event.target.id })
                    }
                  >
                    {ans}
                  </Button>
                );
              })}
            </Col>
          </Row>
        </Container>
        {this.state.show ? (
          <Authenticate
            closePopup={() => {
              this.setState({
                show: !this.state.show,
              });
            }}
            vote={(age, gender, username, has_voted) => {
              this.vote(age, gender, username, has_voted);
              this.setState({
                show: !this.state.show,
              });
            }}
          />
        ) : null}
      </div>
    );
  }
}
