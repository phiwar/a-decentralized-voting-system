const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");

const saltRounds = 10;

let User = new Schema({
  user_username: {
    type: String,
    unique: true,
    minlength: 10,
    maxlength: 10,
    required: true,
  },
  user_password: {
    type: String,
    required: true,
  },
  user_age: {
    type: String,
  },
  user_gender: {
    type: String,
  },
  has_voted: {
    type: [String],
  },
});

User.pre("save", function (next) {
  // Check if document is new or a new password has been set
  if (this.isNew || this.isModified("user_password")) {
    // Saving reference to this because of changing scopes
    const document = this;
    bcrypt.hash(document.user_password, saltRounds, function (
      err,
      hashedPassword
    ) {
      if (err) {
        next(err);
      } else {
        document.user_password = hashedPassword;
        next();
      }
    });
  } else {
    next();
  }
});

User.methods.isCorrectPassword = function (user_password, callback) {
  bcrypt.compare(user_password, this.user_password, function (err, same) {
    if (err) {
      callback(err);
    } else {
      callback(err, same);
    }
  });
};

module.exports = mongoose.model("User", User);
