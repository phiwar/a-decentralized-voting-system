pragma solidity >= 0.4.21 < 0.7.0;

contract Poll {

    // Struct that contains information about a question in a poll
    struct Question {
        uint id;
        string ans;
        uint voteCount;
    }

    // Provides data about a poll
    struct PollData{
        uint id;
        string title;
        uint voteCount;
        uint ansAmount;
    }

    // Provides specific information about a voter
    struct Voter{
        uint id;
        address adrs;
        string gender;
        uint age;
        string vote_alt;
    }

    // Provides information about a vote
    struct VoterMeta{
        uint pid;
        bool hasVoted;
    }

    mapping(uint => Question) public questions;
    mapping(uint => mapping(address => VoterMeta)) public voters;

    // One to many mapping. Each poll id is mapped to many questions contained
    // in a mapping. Each question has a question id.
    mapping(uint => mapping(uint => Question)) public polls;

    // One to many mapping. Each poll is mapped to many voters in a mapping.
    // Each vote has a unique id.
    mapping(uint => mapping(uint => Voter)) private votes;

    // One to one. Mapping containg data about a question, indexed with poll id
    mapping(uint => PollData) public pollData;


    uint public questionsCount; // This isnt used anymore
    uint public pollCount;
    string[] public testQ; // Only used for testing
    string[] public testQ2; // Only used for testing

    constructor () public {
        createPoll("This is a test");
        testQ.push("Yes");
        testQ.push("No");
        testQ.push("Maybe");

        createPoll("This is another test");
        testQ2.push("1");
        testQ2.push("2");
        testQ2.push("3");

        addQuestions(0, testQ);
        addQuestions(1, testQ2);
    }

    function addQuestions (uint id, string[] memory user_questions) private {
        require(user_questions.length <= 10, "Cant add more then 10 question alternatives");
        // questions = polls[id]
        for (uint i = 0; i < user_questions.length; i++) {
            polls[id][i] = Question(i, user_questions[i], 0);
            questionsCount ++;
            pollData[id].ansAmount += 1;
        }
    }

    function addPollQuestion(uint id, uint index, string memory q) public{
        polls[id][index] = Question(index, q, 0);
        pollData[id].ansAmount += 1;
    }

    function createPoll(string memory title) public returns (uint){
        pollData[pollCount].id = pollCount;
        pollData[pollCount].title = title;
        pollData[pollCount].voteCount = 0;
        pollData[pollCount].ansAmount = 0;
        pollCount++;
        // Return created pid
        return pollCount;
    }

    function vote (uint pollId, uint _questionId, string memory gender, uint age) public returns (uint){
        // require that they haven't voted before
        // require(voters[msg.sender].pid == pollId && voters[msg.sender].hasVoted, "already voted");

        // require a valid candidate
        require(_questionId >= 0 && _questionId <= questionsCount, "invalid question index");

        // record that voter has voted
        voters[pollId][msg.sender].hasVoted = true;

        // Store information about voter
        votes[pollId][pollData[pollId].voteCount].id = age;
        votes[pollId][pollData[pollId].voteCount].adrs = msg.sender;
        votes[pollId][pollData[pollId].voteCount].gender = gender;
        votes[pollId][pollData[pollId].voteCount].age = age;
        votes[pollId][pollData[pollId].voteCount].vote_alt = polls[pollId][_questionId].ans;

        // update candidate vote Count
        // questions[_questionId].voteCount ++;
        polls[pollId][_questionId].voteCount ++;
        pollData[pollId].voteCount ++;
        return _questionId;
        //emit votedEvent(_questionId);
    }

    function getAge(uint pid) public view returns (uint[] memory){
        // Only allow votes if sender has voted on the poll being requested
        require(voters[pid][msg.sender].hasVoted, "User must vote to get stats");
        uint[] memory ages = new uint[](pollData[pid].voteCount);
        uint[] memory ans_alts = new uint[](pollData[pid].voteCount);
        for(uint i = 0; i < pollData[pid].voteCount; i++){
            ages[i] = votes[pid][i].age;
            ans_alts[i] = 0;
        }
        return ages;

    }

    function getGender(uint pid) public view returns (uint[] memory, uint[] memory){
        require(voters[pid][msg.sender].hasVoted, "User must vote to get stats");
        uint[] memory genders = new uint[](pollData[pid].voteCount);
        uint[] memory voteAns = new uint[](pollData[pid].voteCount);
        for(uint i = 0; i < pollData[pid].voteCount; i++){
            genders[i] = genderToUint(votes[pid][i].gender);
            voteAns[i] = getQidFromString(pid, votes[pid][i].vote_alt);
        }
        return (genders, voteAns);
    }

    function getAll(uint pid) public view returns (uint[] memory){
        require(voters[pid][msg.sender].hasVoted, "User must vote to get stats");
        uint[] memory ansAlts = new uint[](pollData[pid].voteCount);
        for(uint i = 0; i < pollData[pid].voteCount; i++){
            ansAlts[i] = getQidFromString(pid, votes[pid][i].vote_alt);

        }
        return (ansAlts);
    }

    function getQidFromString(uint pid, string memory str) private view returns(uint){
        for(uint i = 0; i < 10; i++){
            if(keccak256(bytes(str)) == keccak256(bytes(polls[pid][i].ans))){
                return i;
            }
        }
    }

    function genderToUint(string memory g) private pure returns(uint){
        if(keccak256(bytes(g)) == keccak256(bytes('Male'))){
            return 0;
        }
        if(keccak256(bytes(g)) == keccak256(bytes('Female'))){
            return 1;
        }
        if(keccak256(bytes(g)) == keccak256(bytes('Other'))){
            return 2;
        }
        else{
            return 3;
        }
    }
}