import React, { Component } from "react";
import axios from "axios";

export default class EditUser extends Component {
  constructor(props) {
    super(props);

    this.onChangeUserPassword = this.onChangeUserPassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      user_username: "",
      user_password: "",
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:4000/users/" + this.props.match.params.id)
      .then((response) => {
        this.setState({
          user_username: response.data.user_username,
          user_password: response.data.user_password,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  onChangeUserPassword(e) {
    this.setState({
      user_password: e.target.value,
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      user_username: this.state.user_username,
      user_password: this.state.user_password,
    };
    console.log(obj);
    axios
      .post(
        "http://localhost:4000/users/update/" + this.props.match.params.id,
        obj
      )
      .then((res) => console.log(res.data));

    this.props.history.push("/users");
  }

  onRemove(e) {
    e.preventDefault();
    const obj = {
      user_username: this.state.user_username,
      user_password: this.state.user_password,
    };
    console.log("Deleting user: " + obj.user_username);
    axios
      .delete(
        "http://localhost:4000/users/delete/" + this.props.match.params.id,
        obj
      )
      .then((res) => console.log(res.data));

    this.props.history.push("/users");
  }

  render() {
    return (
      <div>
        <h3 align="center">Update user</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Username: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_username}
              disabled="disabled"
            />
          </div>
          <div className="form-group">
            <label>Password: </label>
            <input
              type="text"
              className="form-control"
              onChange={this.onChangeUserPassword}
            />
          </div>
          <br />
          <div className="form-group">
            <input
              type="submit"
              value="Update user"
              className="btn btn-primary"
            />
            <input
              type="submit"
              onClick={(e) => this.onRemove(e)}
              value="Delete"
              className="btn btn-secondary"
            />
          </div>
        </form>
      </div>
    );
  }
}
