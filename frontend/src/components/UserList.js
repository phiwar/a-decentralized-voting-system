import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const User = (props) => (
  <tr>
    <td>{props.user.user_username}</td>
    <td>{props.user.user_password}</td>
    <td>{props.user.user_age}</td>
    <td>{props.user.user_gender}</td>
    <td>
      <Link to={"/edit/" + props.user._id}>Edit</Link>
    </td>
  </tr>
);

export default class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = { users: [] };
  }

  componentDidMount() {
    axios
      .get("http://localhost:4000/users/")
      .then((response) => {
        this.setState({ users: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  componentDidUpdate(prevProps) {
    if (
      //Needs fixing
      prevProps.user_password !== this.props.user_password ||
      prevProps.user_username !== this.props.user_username
    ) {
      axios
        .get("http://localhost:4000/users/")
        .then((response) => {
          this.setState({ users: response.data });
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  userList() {
    return this.state.users.map(function (currentUser, i) {
      return <User user={currentUser} key={i} />;
    });
  }

  render() {
    return (
      <div>
        <h3>List of Persons</h3>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>Personal Number</th>
              <th>Password</th>
              <th>Age</th>
              <th>Gender</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.userList()}</tbody>
        </table>
      </div>
    );
  }
}
