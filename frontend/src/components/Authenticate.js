import React from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { authenticationService } from "../services/authenticationService";

export default class Authenticate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      password: "",
      show: false,
      showAuth: true,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleChangePassword(event) {
    this.setState({ password: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const url = "http://localhost:4000/api/authenticate";
    fetch(url, {
      method: "POST",
      body: JSON.stringify(this.state),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        if (res.status >= 200 && res.status <= 299) {
          return res.json();
        } else {
          alert("Incorrect pincode or personal number");
          throw Error(res.statusText);
        }
      })
      .then((data) => {
        console.log(this.state.value + " was sucessfully authenticated");
        console.log("Logging in user...");
        authenticationService.login(this.state.value, this.state.password);
        this.props.vote(data.age, data.gender, data.username, data.has_voted);
      })
      .catch((error) => console.log(error));
  }

  render() {
    return (
      <div>
        <Modal show={this.state.showAuth}>
          <Modal.Header closeButton onClick={this.props.closePopup}>
            <img src={require("../pics/bankidLogga.jpg")} width={"10%"} />

            <Modal.Title id="auth-title" style={{ marginLeft: "18%" }}>
              Sign with BankID
            </Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="enter-pNumber">
                <Form.Label>Personal number</Form.Label>
                <Form.Control
                  placeholder="Enter personal number"
                  value={this.state.value}
                  onChange={this.handleChange}
                  required
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Personal pincode</Form.Label>
                <Form.Control
                  placeholder=" Enter your pincode"
                  type="password"
                  value={this.state.password}
                  onChange={this.handleChangePassword}
                  required
                ></Form.Control>
              </Form.Group>

              <Form.Text className="text-muted">
                Sign with BankID to validate your vote
              </Form.Text>

              <Form.Group>
                <Button variant="primary" className="float-right" type="submit">
                  Sign vote
                </Button>
                <Button
                  variant="secondary"
                  className="float-left"
                  onClick={this.props.closePopup}
                >
                  Cancel
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
