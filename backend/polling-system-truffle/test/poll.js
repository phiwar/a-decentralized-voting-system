var Poll = artifacts.require("./Poll.sol");

contract("Poll", function(accounts) {
  var pollInstance;

  it("initializes with two questions", function() {
    return Poll.deployed().then(function(instance) {
      return instance.questionsCount();
    }).then(function(count) {
      assert.equal(count, 3);
    });
  });

  it("it initializes the questionss with the correct values", function() {
    return Poll.deployed().then(function(instance) {
      pollInstance = instance;
      return pollInstance.questions(0);
    }).then(function(question) {
      assert.equal(question[0], 0, "contains the correct id");
      assert.equal(question[1], "Yes", "contains the correct name");
      assert.equal(question[2], 0, "contains the correct votes count");
      return pollInstance.questions(1);
    }).then(function(question) {
      assert.equal(question[0], 1, "contains the correct id");
      assert.equal(question[1], "No", "contains the correct name");
      assert.equal(question[2], 0, "contains the correct votes count");
    });
  });

  it("allows a voter to cast a vote on poll", function() {
    return Poll.deployed().then(function(instance) {
      pollInstance = instance;
      answerId = 1;
      return pollInstance.vote(answerId, { from: accounts[0] });
    }).then(function(receipt) {
      return pollInstance.voters(accounts[0]);
    }).then(function(voted) {
      assert(voted, "the voter was marked as voted");
      return pollInstance.questions(answerId);
    }).then(function(question) {
      var voteCount = question[2];
      assert.equal(voteCount, 1, "increments the answers vote count");
    })
  });

  it("throws an exception for invalid answers", function() {
    return Poll.deployed().then(function(instance) {
      pollInstance = instance;
      return pollInstance.vote(99, { from: accounts[1] })
    }).then(assert.fail).catch(function(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      return pollInstance.questions(1);
    }).then(function(question1) {
      var voteCount = question1[2];
      assert.equal(voteCount, 1, "answer 1 did not receive any votes");
      return pollInstance.questions(2);
    }).then(function(question2) {
      var voteCount = question2[2];
      assert.equal(voteCount, 0, "answer 2 did not receive any votes");
    });
  });

  it("throws an exception for double voting", function() {
    return Poll.deployed().then(function(instance) {
      pollInstance = instance;
      questionId = 2;
      pollInstance.vote(questionId, { from: accounts[1] });
      return pollInstance.questions(questionId);
    }).then(function(question) {
      var voteCount = question[2];
      assert.equal(voteCount, 1, "accepts first vote");
      // Try to vote again
      return pollInstance.vote(questionId, { from: accounts[1] });
    }).then(assert.fail).catch(function(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      return pollInstance.questions(1);
    }).then(function(question1) {
      var voteCount = question1[2];
      assert.equal(voteCount, 1, "answer 1 did not receive any votes");
      return pollInstance.questions(2);
    }).then(function(question2) {
      var voteCount = question2[2];
      assert.equal(voteCount, 1, "answer 2 did not receive any votes");
    });
  });
});