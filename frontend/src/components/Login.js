import React from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { authenticationService } from "../services/authenticationService.js";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      password: "",
      show: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleChangePassword(event) {
    this.setState({ password: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const user = authenticationService.login(
      this.state.value,
      this.state.password
    );
    if (user !== null) {
      this.props.closePopup();
    }
  }

  render() {
    return (
      <div>
        <Modal show={!this.state.show}>
          <Modal.Header closeButton onClick={this.props.closePopup}>
            <img src={require("../pics/bankidLogga.jpg")} width={"10%"} />
            <Modal.Title id="auth-title" style={{ marginLeft: "18%" }}>
              Login with BankID
            </Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="enter-pNumber">
                <Form.Label>Personal number</Form.Label>
                <Form.Control
                  placeholder="Enter personal number"
                  value={this.state.value}
                  onChange={this.handleChange}
                  required
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Personal pincode</Form.Label>
                <Form.Control
                  placeholder=" Enter your pincode"
                  type="password"
                  value={this.state.password}
                  onChange={this.handleChangePassword}
                  required
                ></Form.Control>
              </Form.Group>

              <Form.Text className="text-muted">
                Login with BankID to see statistics
              </Form.Text>

              <Form.Group>
                <Button variant="primary" className="float-right" type="submit">
                  Login
                </Button>
                <Button
                  variant="secondary"
                  className="float-left"
                  onClick={this.props.closePopup}
                >
                  Cancel
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
