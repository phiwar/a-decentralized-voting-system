import React, { Component } from "react";
import PollJson from "./Poll.json";
import getWeb3 from "./getWeb3";
import Poll from "../components/Poll";

class Contract extends Component {
  state = {
    storageValue: 0,
    web3: null,
    accounts: null,
    contract: null,
    qCount: 0,
    question: null,
    id: null,
    answers: { 0: [] },
    update: null,
    key: null,
    polls: [],
    titles: ["Dödstraff"],
  };

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = PollJson.networks[networkId];
      const instance = new web3.eth.Contract(
        PollJson.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance }, this.initPolls);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.error(error);
    }
  };

  // Gets titles and questions for poll with poll id: pid
  async getPoll(pid) {
    const { accounts, contract } = this.state;
    var result = { id: 0, title: null, qs: [], voteCount: 0, qVoteCount: [] };
    var done = false;
    // This should initiate a transaction
    for (var i = 0; i <= 10; i++) {
      // polls can be viewed as a 2D array with the rows representing each poll and columns
      // representing the answers. This for-loop will fetch each question from the row given
      // by the poll id (pid)
      await contract.methods
        .polls(pid, i)
        .call()
        .then(function (res) {
          if (res.ans == "") {
            done = true;
          } else {
            result.qs.push(res.ans);
            result.qVoteCount.push(res.voteCount);
          }
        });
      if (done) {
        break;
      }
    }
    // pollData contains data about a poll, its id, title and vote count
    await contract.methods
      .pollData(pid)
      .call()
      .then(function (res) {
        result.id = res.id;
        result.title = res.title;
        result.voteCount = res.voteCount;
      });
    return result;
  }

  // Gett all polls in the contract with ids between 0 and amount
  async getPolls(amount) {
    var polls = [];
    for (var i = 0; i < amount; i++) {
      var poll = await this.getPoll(i);
      polls.push(poll);
    }
    return polls;
  }

  // Casts a vote on a single question by calling the vote() function in contract.sol
  async castVote(pid, qid, gender, age, username) {
    const { accounts, contract } = this.state;
    // This should initiate a transaction
    await contract.methods
      .vote(pid, qid, gender, age)
      .send({ from: String(accounts) })
      .then(function (res) {
        alert("Vote cast!");
      });
  }

  // Gets the all of the questions and the amount of votes on each
  async getStats(pid) {
    const { accounts, contract } = this.state;

    // contains title, voteCount and ansCount
    var pollJSON = await this.getPoll(pid).then((res) => res);

    // This is the format exprected by the Stats component
    var result = {
      tite: pollJSON.title,
      totalVoteCount: pollJSON.voteCount,
      labels: pollJSON.qs,
      datasets: [
        {
          data: pollJSON.qVoteCount.map((i) => parseInt(i)),
          backgroundColor: ["#4b77a9", "#5f255f", "#d21243", "#B27200"],
        },
      ],
    };
    return result;
  }

  // Initate polls in state
  initPolls = async () => {
    const { accounts, contract } = this.state;
    // The getPoll function will loop through the value given to it, fetch all polls
    // between 0 and the amount given and push it into a list of JSONs.
    var tmp = await contract.methods
      .pollCount()
      .call()
      .then((amount) => this.getPolls(amount));

    this.setState({ polls: tmp });
  };

  // Create a new poll with hard coded title and answers
  async create(title, qs) {
    const { accounts, contract } = this.state;
    // List of answers to be provided in the poll
    await contract.methods
      .createPoll(title)
      .send({ from: String(accounts) })
      .then(async function (res) {
        const pid = await contract.methods
          .pollCount()
          .call()
          .then((id) => id - 1);
        for (var i = 0; i < qs.length; i++) {
          contract.methods
            .addPollQuestion(pid, i, qs[i])
            .send({ from: String(accounts) })
            .then(function (res) {
              console.log(qs[i] + " added into poll id: " + pid);
            });
        }
      });
  }

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div>
        <div>
          {this.state.polls.map((poll) => (
            <Poll
              question={poll.title}
              id={poll.id}
              answers={poll.qs}
              update={() => this.getStats(poll.id)}
              key={0}
              voteFunction={(pid, qid, g, a) => this.castVote(pid, qid, g, a)}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Contract;
