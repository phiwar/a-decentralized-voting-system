import { BehaviorSubject } from "rxjs";
import { handleResponse } from "./handleResponse";

const currentUserSubject = new BehaviorSubject(
  JSON.parse(localStorage.getItem("currentUser"))
);

export const authenticationService = {
  login,
  logout,
  currentUser: currentUserSubject.asObservable(),
  get currentUserValue() {
    return currentUserSubject.value;
  },
};

function login(username, password) {
  console.log("Attempting to log in user " + username);
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ username, password }),
  };

  return fetch("http://localhost:4000/api/login", requestOptions)
    .then(handleResponse)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("currentUser", JSON.stringify(user));
      currentUserSubject.next(user);
      console.log(username + " was successfully logged in");
      return user;
    });
}

function logout() {
  // remove user from local storage to log user out
  console.log("Logging out current user");
  localStorage.removeItem("currentUser");
  currentUserSubject.next(null);
}
