import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

import "../css/dashboard.css";

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: null,
      ansAlts: [""],
    };
    this.createFunction = props.createFunction;
    // To call 'this.something' in eventhandler bind functions with 'this'
    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAnsChange = this.handleAnsChange.bind(this);
    this.addAnswer = this.addAnswer.bind(this);
    this.rmAnswer = this.rmAnswer.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleAnsChange(event) {
    const target = event.target;
    const value = target.value;
    const key = target.name.split("-")[1];
    // Update value
    this.state.ansAlts[key] = value;
    //this.setState({ansAlts: this.state.ansAlts});
  }

  addAnswer() {
    // alert(this.state.ansAlts);
    if (this.state.ansAlts.length < 10) {
      this.state.ansAlts.push("");
      this.setState({ ansAlts: this.state.ansAlts });
    }
  }
  rmAnswer() {
    // alert(this.state.ansAlts);
    if (this.state.ansAlts.length > 1) {
      this.state.ansAlts.pop();
      this.setState({ ansAlts: this.state.ansAlts });
    }
  }

  submit(event) {
    event.preventDefault();
    // alert(JSON.stringify(this.state));
    this.createFunction(this.state.title, this.state.ansAlts);
    // alert(this.state.title);
  }

  render() {
    return (
      <div>
        <div style={{ textAlign: "center" }}>
          <h1>Admin page</h1>
          <h2>Here you can create your own poll</h2>
        </div>
        <Form>
          <Form.Group>
            <Form.Label>Poll question</Form.Label>
            <Form.Control
              name="title"
              type="text"
              placeholder="Write your question here"
              onChange={this.handleChange}
            />
          </Form.Group>
          <Button onClick={this.addAnswer}>Add alternative</Button>
          <Button onClick={this.rmAnswer}>Remove alternative</Button>
          <Form.Group>
            <Form.Label>Alternatives</Form.Label>
            {this.state.ansAlts.map((alt, i) => {
              return (
                <Form.Control
                  key={i}
                  name={"alt-" + i}
                  type="text"
                  placeholder={"Alternative " + i}
                  onChange={this.handleAnsChange}
                />
              );
            })}
          </Form.Group>
          <input type="submit" value="Create" onClick={this.submit} />
        </Form>
        {/* <Button style={{ alignContent: "center" }}>Create poll</Button> */}
      </div>
    );
  }
}
export default Admin;
