import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { authenticationService } from "../services/authenticationService";
import Login from "../components/Login";

export default class NavbarCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLogin: false,
    };
  }

  closePopup = () => {
    this.setState({
      showLogin: false,
    });
  };

  render() {
    const loginOrOut = () => {
      if (this.props.currentUser == null) {
        return (
          <Nav.Link
            onClick={() =>
              this.setState({
                showLogin: true,
              })
            }
          >
            Login
          </Nav.Link>
        );
      } else {
        return (
          <Nav.Link
            onClick={() => {
              authenticationService.logout();
              alert("You have been logged out.");
            }}
          >
            Log out
          </Nav.Link>
        );
      }
    };

    return (
      <div>
        <Navbar
          className="dashboard-navbar"
          collapseOnSelect
          expand="lg"
          bg="dark"
          variant="dark"
        >
          <Navbar.Brand id="brandname" href="/">
            GlobalOpinionPoll
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/admin">Admin</Nav.Link>
              <Nav.Link href="/users">Users</Nav.Link>
              <Nav.Link href="/faq">FAQ</Nav.Link>
              {loginOrOut()}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        {this.state.showLogin ? <Login closePopup={this.closePopup} /> : null}
      </div>
    );
  }
}
