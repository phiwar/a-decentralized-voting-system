# Initialisation

New dependencies might have been introduced since you last pulled the repository. If you try to execute the code without installing these you will get non-descriptive errors. Therefore, the first thing you should do after pulling is execute: `npm install` to install these.

# Structure

When implementing a new front-end feature, create a new React component, then add this component to App.js or in another suitable component.

# Setting up the database

1. Make sure you have all the dependencies included by running `npm install` in both the ~/frontend and ~/backend directories.

2. In the ~/backend directory, start server by typing `$ nodemon server`. You should see the following output:

```
[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node server.js`
Server is running on Port: 4000
```

3. Make sure MongoDB is installed. For Mac, use `$ brew install mongodb`. For Linux or Windows, follow the instructions on https://docs.mongodb.com/manual/administration/install-community/.

4. In a new termianl, create a directory for MongoDB to store the data with the command `$ mkdir -p /data/db`. Make sure the account you're using has read and write access to this directory.

5. Start up MongoDB by executing `$ mongod`. The database server is now running on port 27017 and is waiting to accept client connections.

6. In a new terminal, connect to the database server by executing `$ mongo`. Create a new database with the name _users_ by typing `> use users`.

7. In a new terminal, go to ~/frontend and start project with `npm start`. You should now be able to read and write to this database on the User database frontend page.
