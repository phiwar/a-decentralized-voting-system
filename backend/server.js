const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const userRoutes = express.Router();
const PORT = 4000;
const cookieParser = require("cookie-parser");
let User = require("./user.model.js");

app.use(cors());
app.use(bodyParser.json());
app.use("/users", userRoutes);
app.use(cookieParser());

mongoose.connect("mongodb://127.0.0.1:27017/users", { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once("open", function () {
  console.log("MongoDB database connection established successfully");
});

// Route to get all users
userRoutes.route("/").get(function (req, res) {
  User.find(function (err, users) {
    if (err) {
      console.log(err);
    } else {
      res.json(users);
    }
  });
});

// Route to find user
userRoutes.route("/:id").get(function (req, res) {
  let id = req.params.id;
  User.findById(id, function (err, user) {
    res.json(user);
  });
});

// Route for updating user
userRoutes.route("/update/:id").post(function (req, res) {
  User.findById(req.params.id, function (err, user) {
    if (!user) res.status(404).send("data is not found");
    else user.user_username = user.user_username;
    user.user_password = req.body.user_password;

    user
      .save()
      .then((user) => {
        res.json("User successfully updated");
      })
      .catch((err) => {
        res.status(400).send("Error: " + err + ". Update not possible");
      });
  });
});

// Route for deleting user
userRoutes.route("/delete/:id").delete(function (req, res) {
  User.findById(req.params.id, function (err, user) {
    if (!user) res.status(404).send("data is not found");
    else user.delete;

    user
      .delete()
      .then((user) => {
        res.json("User successfully deleted");
      })
      .catch((err) => {
        res
          .status(400)
          .send("Error: " + err + ". Deletion of user not possible");
      });
  });
});

// Route for adding user
userRoutes.route("/add").post(function (req, res) {
  let user = new User(req.body);
  console.log(user);
  user
    .save()
    .then((user) => {
      res.status(200).json({ user: "user added successfully" });
    })
    .catch((err) => {
      res.status(400).send("Error: + " + err + ". Adding new user failed");
    });
});

// Route for checking if user has already voted on a question
app.post("/hasVoted", function (req, res) {
  const user_username = req.body.value;
  const pollId = req.body.pollId;
  User.findOne({ user_username }, function (err, user) {
    if (err) {
      console.error(err);
      res.status(500).json({
        error: "Internal error please try again",
      });
    } else if (!user) {
      res.status(401).json({
        error: "Couldn't find user.",
      });
    } else {
      user.has_voted = user.has_voted.concat(pollId); //Adds the pollId to the users "has voted"
      user.save().then(res.sendStatus(200));
    }
  });
});

// Route for authenticating a user
app.post("/api/authenticate", function (req, res) {
  const user_password = req.body.password;
  const user_username = req.body.value;
  User.findOne({ user_username }, function (err, user) {
    if (err) {
      console.error(err);
      res.status(500).json({
        error: "Internal error please try again",
      });
    } else if (!user) {
      res.status(401).json({
        error: "Couldn't find user.",
      });
    } else {
      user.isCorrectPassword(user_password, function (err, same) {
        if (err) {
          console.log("1");
          res.status(500).json({
            error: "Internal error please try again",
          });
        } else if (!same) {
          res.status(401).json({
            error: "Incorrect password.",
          });
        } else {
          res.json({
            age: user.user_age,
            gender: user.user_gender,
            has_voted: user.has_voted,
            username: user.user_username,
          });
        }
      });
    }
  });
});

// Route for logging in a user
app.post("/api/login", function (req, res) {
  const user_password = req.body.password;
  const user_username = req.body.username;
  console.log(req.body.password);
  console.log(req.body.username);
  User.findOne({ user_username }, function (err, user) {
    if (err) {
      console.error(err);
      res.status(500).json({
        error: "Internal error please try again",
      });
    } else if (!user) {
      res.status(401).json({
        error: "Couldn't find user.",
      });
    } else {
      user.isCorrectPassword(user_password, function (err, same) {
        if (err) {
          res.status(500).json({
            error: "Internal error please try again",
          });
        } else if (!same) {
          res.status(401).json({
            error: "Incorrect password.",
          });
        } else {
          res.json({
            token: "webtoken",
            username: user.user_username,
          });
        }
      });
    }
  });
});

app.listen(PORT, function () {
  console.log("Server is running on Port: " + PORT);
});
