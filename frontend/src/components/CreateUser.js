import React, { Component } from "react";
import axios from "axios";

export default class CreateUser extends Component {
  constructor(props) {
    super(props);

    this.onChangeUserPassword = this.onChangeUserPassword.bind(this);
    this.onChangeUserGender = this.onChangeUserGender.bind(this);
    this.onChangeUserUsername = this.onChangeUserUsername.bind(this);
    this.onChangeUserAge = this.onChangeUserAge.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      user_username: "",
      user_password: "",
      user_age: "",
      user_gender: "",
      has_voted: [],
    };
  }

  onSubmit(e) {
    e.preventDefault();

    console.log(`User added:`);
    console.log(`Username: ${this.state.user_username}`);
    console.log(`Password: ${this.state.user_password}`);

    const newUser = {
      user_username: this.state.user_username,
      user_password: this.state.user_password,
      user_age: this.state.user_age,
      user_gender: this.state.user_gender,
      has_voted: this.state.has_voted,
    };

    axios
      .post("http://localhost:4000/users/add", newUser)
      .then((res) => console.log(res.data));

    this.state = {
      user_username: "",
      user_password: "",
      user_age: "",
      user_gender: "",
      has_voted: [],
    };
    this.props.history.push("/users");
  }

  onChangeUserUsername(e) {
    this.setState({
      user_username: e.target.value,
    });
  }

  onChangeUserPassword(e) {
    this.setState({
      user_password: e.target.value,
    });
  }

  onChangeUserGender(e) {
    this.setState({
      user_gender: e.target.value,
    });
  }

  onChangeUserAge(e) {
    this.setState({
      user_age: e.target.value,
    });
  }

  render() {
    return (
      <div style={{ marginTop: 10 }}>
        <h3>Create new user</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Username: </label>
            <input
              type="text"
              required
              placeholder="10 digits"
              className="form-control"
              value={this.state.user_username}
              onChange={this.onChangeUserUsername}
            />
          </div>
          <div className="form-group">
            <label>Password: </label>
            <input
              type="password"
              required
              className="form-control"
              value={this.state.user_password}
              onChange={this.onChangeUserPassword}
            />
          </div>
          <div className="form-group">
            <label>Age: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.user_age}
              onChange={this.onChangeUserAge}
            />
          </div>

          <label>Gender:</label>
          <div className="form-group">
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                id="genderMale"
                value="Male"
                required
                checked={this.state.user_gender === "Male"}
                onChange={this.onChangeUserGender}
              />
              <label className="form-check-label">Male</label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                id="genderFemale"
                value="Female"
                checked={this.state.user_gender === "Female"}
                onChange={this.onChangeUserGender}
              />
              <label className="form-check-label">Female</label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                id="genderNonBinary"
                value="Non Binary"
                checked={this.state.user_gender === "Non Binary"}
                onChange={this.onChangeUserGender}
              />
              <label className="form-check-label">Non Binary</label>
            </div>
          </div>

          <div className="form-group">
            <input
              type="submit"
              value="Create user"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}
