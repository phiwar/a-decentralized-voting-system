import React from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import "../css/dashboard.css";

export default class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            faq: "", 
            howTo: ""
        }
    }

    whatisthis(){
        return(
            <div>
                <h4>What is this?</h4>
                <p>
                    GlobalOpinionPoll is a global opinion poll.
                </p>
            </div>
        )
    }
    security(){
        return(
            <div>
                <h4>How do I know that its secure?</h4>
                <p>
                    Because is said so.
                </p>
            </div>
        )
    }
    aboutUs(){
        return(
            <div>
                <h4>Who built this?</h4>
                <p>
                    ...
                </p>
            </div>
        )
    }
    vote(){
        return(
            <div>
                <h4>How to vote</h4>
                <p>
                    ...
                </p>
            </div>
        )
    }
    create(){
        return(
            <div>
                <h4>How to create a poll</h4>
                <p>
                    ...
                </p>
            </div>
        )
    }
    viewStats(){
        return(
            <div>
                <h4>How to view statistics</h4>
                <p>
                    ...
                </p>
            </div>
        )
    }

    render(){
        return(
            <div>
                <Row>
                    <Col>
                        <h1>Welcome to <b>GlobalOpinionPoll</b></h1>
                        <p>The first decentralized opinion poll in history.</p>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h3>2 polls 3 votes cast</h3>
                    </Col>
                </Row>
                
                <Row className="faq-row text-align-left">
                    <Col md={12}>
                        <h1>FAQ</h1>
                    </Col>
                    <Col className="card" onClick={() => this.setState({faq: this.whatisthis()})}>
                        <h4>What is this?</h4>
                    </Col>
                    <Col className="card" onClick={() => this.setState({faq: this.security()})}>
                        <h4>How do I know that its secure?</h4>
                    </Col>
                    <Col className="card" onClick={() => this.setState({faq: this.aboutUs()})}>
                        <h4>Who built this?</h4>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="text-align-left align-content">{this.state.faq}</p>
                    </Col>
                </Row>
                <Row className="faq-row text-align-left">
                    <Col md={12}>
                        <h1>How to...</h1>
                    </Col>
                    <Col className="card" onClick={() => this.setState({howTo: this.vote()})}>
                        <h4>Vote</h4>
                    </Col>
                    <Col className="card" onClick={() => this.setState({howTo: this.create()})}>
                        <h4>Create polls</h4>
                    </Col>
                    <Col className="card" onClick={() => this.setState({howTo: this.viewStats()})}>
                        <h4>View statistics</h4>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="text-align-left align-content">{this.state.howTo}</p>
                    </Col>
                </Row>
            </div>
        )
    }
}
