import React, { Component } from "react";
import { Bar, Line, Pie } from "react-chartjs-2";
import { Button, Modal, ModalBody } from "react-bootstrap";

export default class Stats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statsData: () => this.props.update(),
    };
  }

  async componentDidMount() {
    var tmp = await this.state.statsData().then((res) => res);
    this.setState({ statsData: tmp });
  }

  static defaultProps = {
    displayTitle: true,
    displayLegend: true,
    legendPosition: "right",
  };

  render() {
    return (
      <div>
        <Modal show={true} size="xl">
          <Modal.Body>
            <Pie
              // data={this.state.ansVoteCount}
              data={this.state.statsData}
              options={{
                title: {
                  display: this.props.displayTitle,
                  text: "The Final Results",
                  fontSize: 25,
                },
                legend: {
                  display: this.props.displayLegend,
                  text: "Number",
                  position: this.props.legendPosition,
                },
                cutoutPercentage: 0,
              }}
            />

            <Bar
              data={this.state.statsData}
              options={{
                title: {
                  display: this.props.displayTitle,
                  fontSize: 25,
                },
                legend: {
                  display: this.props.displayLegend,
                  text: "Number",
                  position: this.props.legendPosition,
                },
              }}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.closePopup}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
